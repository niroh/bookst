<?php

namespace App\Http\Controllers;
use App\Book;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;


class BookstController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $books = Book::all() ;
        //return view('books.index', ['books'=>$books]);
        //$id = 1; //or later change to 2
        $id=Auth::id();
        //$books = User::find($id)->books;
        return view('books.index', compact('books')); 

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Gate::denies('manager')) {
           abort(403,"Sorry you are not allowed to create books..");
            } 
          return view ('books.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
        {
         
            if (Gate::denies('manager')) {
                abort(403,"Sorry you are not allowed to create books..");
                 } 
                 $v = Validator::make($request->all(), [
                    'title' => 'required|string|max:20',
                    'author' => 'required|string|max:20',
                    ]);
                       
                    if ($v->fails()) {
                         return redirect()->back()->withErrors($v->errors());
                     }
                 
            $book = new Book();
            //$id = 1;
            $id=Auth::id();
            $book->title = $request->title;
            $book->author  = $request->author;
            $book->user_id = $id;
            $book->save();
            return redirect('books');
        }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Gate::denies('manager')) {
            abort(403,"Sorry you are not allowed to edit books..");
             } 
        $book = Book::find($id);
        return view('books.edit', compact('book'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $book = Book::find($id);
        if (Gate::denies('manager')){
        if($book->status==0){
            if(!$book->user->id == Auth::id()) return(redirect('books'));
            $book -> update($request->except(['_token']));
            if($request->ajax()){
                return Response::json(array('result'=>'success', 'status'=>$request->status),200);
            }

         }
         return redirect('books');
        }
        if(!$book->user->id == Auth::id()) return(redirect('books'));
            $book -> update($request->except(['_token']));
            if($request->ajax()){
                return Response::json(array('result'=>'success', 'status'=>$request->status),200);
            }
        return redirect('books');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $book = Book::find($id);
        $book -> delete();
        return redirect('books');
    }
}
