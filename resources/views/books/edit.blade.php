<h1>Edit a Book</h1>
<form method = 'post' action="{{action('BookstController@update', $book->id)}}">
@csrf
@method('PATCH')
<div class = "form-group">
    <label for = "title">Book to Update:</label>
    <br> <br>
    <input type= "text" class = "form-control" name= "title" value = "{{$book->title}}">
    <br> <br>
    <input type= "text" class = "form-control" name= "author" value = "{{$book->author}}">
</div>
<br>
<div class = "form-group">
    <input type ="submit" class = "form-control" name="submit" value ="Save">
</div>

</form>

<form method = 'post' action="{{action('BookstController@destroy', $book->id)}}">
@csrf
@method('DELETE')
<br>
<div class = "form-group">
    <input type ="submit" class = "form-control" name="submit" value ="Delete Book">
</div>

</form>
