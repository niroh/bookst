<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
                [
                        'name' => 'Matan Hamburger',
                        'email' => 'matan@gmail.com',
                        'password' => Hash::make('12345678'),
                        'created_at' => date('Y-m-d G:i:s'),
                        'role' => 'manager'
                ],
                [
                        'name' => 'Akiva Jacobs',
                        'email' => 'akiva@gmail.com',
                        'password' => Hash::make('12345678'),
                        'created_at' => date('Y-m-d G:i:s'),
                        'role' =>'manager'
                ],
                [
                        'name' => 'Kobi Dadon',
                        'email' => 'kobi@gmail.com',
                        'password' => Hash::make('12345678'),
                        'created_at' => date('Y-m-d G:i:s'),
                        'role' =>'manager'
                ],
                [
                        'name' => 'Nir Ohayon',
                        'email' => 'nir@gmail.com',
                        'password' => Hash::make('12345678'),
                        'created_at' => date('Y-m-d G:i:s'),
                        'role' => 'reader'
                ],
            
                    ]);
    }

}
