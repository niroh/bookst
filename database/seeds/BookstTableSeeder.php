<?php

use Illuminate\Database\Seeder;

class BookstTableSeeder extends Seeder

{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        {
            DB::table('books')->insert(
    [
        [
                'title' => 'Book 1',
                'author' => 'Author 1',
                'user_id' => 1,
                'created_at' => date('Y-m-d G:i:s'),
        ],
        [
                'title' => 'Book 2',
                'author' => 'Author 2',
                'user_id' => 2,
                'created_at' => date('Y-m-d G:i:s'),
        ],
        [
                'title' => 'Book 3',
                'author' => 'Author 3',
                'user_id' => 1,
                'created_at' => date('Y-m-d G:i:s'),
        ],
    
            ]);
        }
    
    }
}
